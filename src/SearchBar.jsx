import * as R from 'ramda';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { reduxForm, Field } from 'redux-form';
import { fetchWeather } from './actions';

class SearchBar extends Component {
    onSubmit = async ({ query }) => {
        await this.props.fetchWeather(query);
    };

    render() {
        const { handleSubmit } = this.props;
        return (
            <form
                className="SearchBar form"
                onSubmit={handleSubmit(this.onSubmit)}
            >
                <div className="form-item">
                    <div className="append">
                        <Field
                            className="search"
                            name="query"
                            placeholder="Enter a city"
                            component={renderField}
                            type="text"
                        />
                        <button className="button-primary">Default</button>
                    </div>
                </div>
            </form>
        );
    }
}

const renderField = ({
    input,
    type,
    placeholder,
    meta: { touched, error, warning },
    className,
}) => (
    <div>
        <input
            {...input}
            className={className}
            placeholder={placeholder}
            type={type}
        />
        {touched &&
            ((error && <span>{error}</span>) ||
                (warning && <span>{warning}</span>))}
    </div>
);

export default R.compose(
    reduxForm({
        form: 'weather-search',
        validate: values => {
            const errors = {};

            if (!values.query || /^\s*$/.test(values.query)) {
                errors.query = 'Search query cannot be blank.';
            }

            return errors;
        },
    }),
    connect(undefined, { fetchWeather }),
)(SearchBar);
