import { combineReducers } from 'redux';
import { reducer as form } from 'redux-form';
import weather from './weather';

export default combineReducers({
    weather,
    form,
});
