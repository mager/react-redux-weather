import * as R from 'ramda';

export default (state = [], action) => {
    console.log(action);
    switch (action.type) {
        case 'FETCH_WEATHER_SUCCESS':
            /* Clean up the data returned */
            const cityName = action.payload.city.name;
            const data = R.compose(
                R.map(R.prop('temp')),
                R.map(R.prop('main')),
            )(action.payload.list);

            return [...state, { cityName, data }];
        default:
            return state;
    }
};
