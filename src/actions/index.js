import axios from 'axios';

const API_KEY = '5a023f7ec835d7de0baa824f83ecd734';
const BASE_URL = 'http://api.openweathermap.org/data/2.5/forecast';
const REQUEST_URL = `${BASE_URL}?appid=${API_KEY}`;

export const fetchWeather = city => async dispatch => {
    /* Fetch some weather data */
    const url = `${REQUEST_URL}&q=${city}&country=US`;
    console.log(url);
    let response = await axios.get(url);
    console.log(response);

    dispatch({
        type: 'FETCH_WEATHER_SUCCESS',
        payload: response.data,
    });
};
