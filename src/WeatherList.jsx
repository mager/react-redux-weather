import React, { Component } from 'react';
import { connect } from 'react-redux';

import Chart from './Chart.jsx';

class WeatherList extends Component {
    componentDidMount() {
        console.log(this.props);
    }

    renderWeather = () => {
        const { weather } = this.props;

        return weather.map(({ cityName, data }) => {
            console.log(data);
            return (
                <tr key={cityName}>
                    <td>{cityName}</td>
                    <td>
                        <Chart data={data} />
                    </td>
                </tr>
            );
        });
    };

    render() {
        return (
            <div className="WeatherList">
                <table>
                    <thead>
                        <tr>
                            <th>City</th>
                            <th>Data</th>
                        </tr>
                    </thead>
                    <tbody>{this.renderWeather()}</tbody>
                </table>
            </div>
        );
    }
}

const mapStateToProps = ({ weather }) => {
    return { weather };
};

export default connect(mapStateToProps, undefined)(WeatherList);
