import React, { Component } from 'react';
import SearchBar from './SearchBar.jsx';
import WeatherList from './WeatherList.jsx';
import './App.css';

const FAKE_WEATHER_DATA = [
    {
        uuid: '1234',
        city: 'SF',
        temp: 88,
    },
];

class App extends Component {
    render() {
        return (
            <div className="App">
                <div className="container">
                    <h1 className="title">Weather Demo</h1>
                    <SearchBar />
                    <WeatherList fakeWeather={FAKE_WEATHER_DATA} />
                </div>
            </div>
        );
    }
}

export default App;
