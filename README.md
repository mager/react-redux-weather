# React Redux Weather Demo

## Installation & Development

```sh
brew install yarn
npx create-react-app react-redux-weather
cd react-redux-weather
yarn
yarn start
```

## Install Dependencies

```sh
yarn add axios react-redux redux redux-form redux-thunk redux-logger ramda react-sparklines
```

## The Redux Flow

![](img/ss-1.png)
